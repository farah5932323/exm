def add_numbers():
    # Logical bug: addition result is not returned or used
    num1 = input("Enter first number: ")
    num2 = input("Enter second number: ")
    result = int(num1) + int(num2)
    print("The result is: ", result)

def list_operations():
    # Syntax and logical bugs
    my_list = [1, 2, 3, 4, 5]
    print(my_list[5])  # IndexError: list index out of range

def string_manipulation():
    # Runtime error due to type issues
    user_input = input("Enter a number: ")
    print("Reversed input: " + user_input[::-1])  # Bug if input is not convertible

def calculate_average():
    # Division by zero error potential
    numbers = input("Enter numbers separated by spaces: ").split()
    numbers = [int(num) for num in numbers]
    average = sum(numbers) / len(numbers)  # ZeroDivisionError if no numbers are entered
    print("Average is:", average)

def file_operations():
    # File handling without exception management
    file = open('non_existent_file.txt', 'r')  # FileNotFoundError potential
    print(file.read())

# Main function to run all buggy functions
def main():
    add_numbers()
    list_operations()
    string_manipulation()
    calculate_average()
    file_operations()

if __name__ == "__main__":
    main()
